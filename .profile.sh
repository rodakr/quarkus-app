export MAVEN_HOME=/usr/local/Cellar/maven/3.6.2
export JAVA_HOME=/Library/Java/JavaVirtualMachines/graalvm-ce-19.2.0.1/Contents/Home
export PATH=$JAVA_HOME/bin:$MAVEN_HOME/bin:$PATH
